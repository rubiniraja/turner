﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Turner.Entities;
using Turner.TitleSearch.Models;
using Turner.UI.Repository;

namespace Turner.TitleSearch.Controllers
{
    /// <summary>
    /// Controller handling title operations
    /// </summary>
    public class TitleController : Controller
    {
        #region Private/protected fields

        /// <summary>
        /// The _repository
        /// </summary>
        protected readonly ITitleRepository _repository;
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleController"/> class.
        /// </summary>
        public TitleController()
        {
            _repository = new TitleRepository();
        }
        #endregion
        /// <summary>
        /// Titles this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Title()
        {

            try
            {
                var titlesModel = new TitleModel();
                return View(titlesModel);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Searches the name of the title by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="jtStartIndex">Start index of the jt.</param>
        /// <param name="jtPageSize">Size of the jt page.</param>
        /// <param name="jtSorting">The jt sorting.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SearchTitleByName(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {

                TitleSearchCriteria titleSearch = new TitleSearchCriteria() { Field = jtSorting, StartIndex = jtStartIndex ,
                                                                              
                                                                              Number = jtPageSize,
                                                                              SearchField = name,
                };
                var titleDetails = _repository.SearchTitles(titleSearch);
              
                return Json(new { Result = "OK", Records = titleDetails.TitleDetails, TotalRecordCount = titleDetails.TotalSearchCount, JsonRequestBehavior.AllowGet });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Awardses the list.
        /// </summary>
        /// <param name="titleId">The title identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AwardsList(int titleId)
        {
            try
            {
                var awardsList = _repository.GetAwardsByTitle(titleId);
                return Json(new { Result = "OK", Records = awardsList });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Others the names list.
        /// </summary>
        /// <param name="titleId">The title identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult OtherNamesList(int titleId)
        {
            try
            {
                var otherNames = _repository.GetOtherNamesByTitle(titleId);
                return Json(new { Result = "OK", Records = otherNames });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Participants the list.
        /// </summary>
        /// <param name="titleId">The title identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ParticipantList(int titleId)
        {
            try
            {
                var participants = _repository.GetParticipantsByTitle(titleId);
                return Json(new { Result = "OK", Records = participants });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Genreses the list.
        /// </summary>
        /// <param name="titleId">The title identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GenresList(int titleId)
        {
            try
            {
                var genres = _repository.GetGenresByTitle(titleId);
                return Json(new { Result = "OK", Records = genres });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        /// <summary>
        /// Stories the lines list.
        /// </summary>
        /// <param name="titleId">The title identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult StoryLinesList(int titleId)
        {
            try
            {
                var storyLines = _repository.GetStoryLinesByTitle(titleId);
                return Json(new { Result = "OK", Records = storyLines });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
 

    }
}
