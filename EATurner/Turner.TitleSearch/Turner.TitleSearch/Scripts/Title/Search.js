﻿/*global $, jQuery, document*/
//Prepare jtable plugin
$(document).ready(function () {
    "use strict";
    $('#BtnTitleSearch').click(function () {
        $('#TitlesTable').jtable('load', {
            name: $('#SearchString').val()
        });
    });
    $('#BtnClearSearch').click(function () {
        $('#SearchString').val("");
        $('#TitlesTable').jtable('load', {
            name: ""
        });
    });
    $('#TitlesTable').jtable({
        title: 'Titles',
        contentType: "application/json; charset=utf-8",
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'TitleName ASC',
        actions: {
            listAction: '/Title/SearchTitleByName'
        },
        recordsLoaded: function () {
            $("#ui-jtable-loading").hide();
        },
        fields: {
            TitleId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            TitleName: {
                title: 'Name',
                width: '23%'
            },
            ReleaseYear: {
                title: 'Release Year',
                list: true,
                width: '15%'
            },
            TitleNameSortable: {
                title: 'SortableName',
                width: '23%',
                list: false
            },
            ProcessedDateTimeUTC: {
                title: 'ProcessedDateTimeUTC',
                width: '12%',
                list: false
            },
            Awards: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (titleInfo) {
                    //debugger;
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../images/misc/award.png" title="Awards" class="imgCls" />');
                    //Open child table when user clicks the image
                    $img.click(function () {

                        $('#TitlesTable').jtable('openChildTable',
                            $img.closest('tr'), {
                                title: 'Awards',
                                actions: {
                                    listAction: '/Title/AwardsList?titleId=' + titleInfo.record.TitleId
                                },
                                fields: {
                                    AwardWon: {
                                        title: 'Award won'

                                    },
                                    AwardYear: {
                                        title: 'Award Year'
                                    },
                                    AwardCompany: {
                                        title: 'Award Company',
                                        width: '30%'

                                    },
                                    Id: {
                                        title: 'Id',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            OtherNames: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (titleInfo) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../images/misc/othername.png" title="other names" class="imgCls" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#TitlesTable').jtable('openChildTable',
                            $img.closest('tr'), {
                                title: 'Other Names',
                                actions: {
                                    listAction: '/Title/OtherNamesList?titleId=' + titleInfo.record.TitleId
                                },

                                fields: {
                                    Language: {
                                        title: 'Language'

                                    },
                                    Type: {
                                        title: 'Type'
                                    },
                                    Name: {
                                        title: 'Name',
                                        width: '30%'

                                    },
                                    Id: {
                                        title: 'Id',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            StoryLines: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (titleInfo) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../images/misc/storyLine.png" title="story Line" class="imgCls" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#TitlesTable').jtable('openChildTable',
                            $img.closest('tr'), {
                                title: 'Story Lines',
                                actions: {
                                    listAction: '/Title/StoryLinesList?titleId=' + titleInfo.record.TitleId
                                },

                                fields: {
                                    Type: {
                                        title: 'Type'

                                    },
                                    Language: {
                                        title: 'Language'
                                    },
                                    Description: {
                                        title: 'Description',
                                        width: '30%'

                                    },
                                    Id: {
                                        title: 'Id',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            TitleGenres: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (titleInfo) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../images/misc/genre.png" title="genres" class="imgCls" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#TitlesTable').jtable('openChildTable',
                            $img.closest('tr'), {
                                title: 'Genres',
                                actions: {
                                    listAction: '/Title/GenresList?titleId=' + titleInfo.record.TitleId
                                },
                                fields: {
                                    Id: {
                                        title: 'Id',
                                        list: false
                                    },
                                    Name: {
                                        title: 'Genre Name',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            TitleParticipants: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (titleInfo) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../images/misc/participant.png" title="participants" class="imgCls" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#TitlesTable').jtable('openChildTable',
                            $img.closest('tr'), {
                                title: 'Participants',
                                actions: {
                                    listAction: '/Title/ParticipantList?titleId=' + titleInfo.record.TitleId
                                },

                                fields: {
                                    Type: {
                                        title: 'Type'

                                    },
                                    Name: {
                                        title: 'Name'
                                    },
                                    IsKey: {
                                        title: 'Is Key Participant',
                                        width: '30%'
                                    },
                                    RoleType: {
                                        title: 'Role',
                                        width: '30%'
                                    },
                                    IsOnScreen: {
                                        title: 'On Screen'
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            }

        }
    });

    $('#TitlesTable').jtable('load');
});