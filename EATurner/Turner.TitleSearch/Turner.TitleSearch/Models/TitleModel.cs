﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Turner.Entities;

namespace Turner.TitleSearch.Models
{
    public class TitleModel
    {
        public TitleModel()
        {
            ItemsPerPage = new List<int>();
            ItemsPerPage.Add(5);
            ItemsPerPage.Add(10);
            ItemsPerPage.Add(15);
        }
        public TitleDetails Titles { get; set; }
        public string UserName { get; set; }
        public List<int> ItemsPerPage { get; set; }
    }
}