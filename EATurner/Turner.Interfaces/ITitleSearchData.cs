﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// ITitleSearchData.cs
// Contains the operations exposed by Title Search data component
#endregion 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turner.Entities;
using Turner.Entities.Title;

namespace Turner.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITitleSearchData
    {
        /// <summary>
        /// Searches the title.
        /// </summary>
        /// <param name="SearchCriteria">The search criteria.</param>
        /// <returns></returns>
        TitleSearchResult SearchTitle(TitleSearchCriteria SearchCriteria);

        /// <summary>
        /// Gets the awards by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        List<Award> GetAwardsByTitle(int TitleId);

        /// <summary>
        /// Gets the story lines by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        List<StoryLine> GetStoryLinesByTitle(int TitleId);

        /// <summary>
        /// Gets the genres by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        List<TitleGenre> GetGenresByTitle(int TitleId);

        /// <summary>
        /// Gets the participants by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        List<TitleParticipant> GetParticipantsByTitle(int TitleId);

        /// <summary>
        /// Gets the other names by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        List<OtherName> GetOtherNamesByTitle(int TitleId);

    }
}
