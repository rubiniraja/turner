﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turner.Data.Model;
using Turner.Entities;
using Turner.Entities.Title;
using Title = Turner.Entities.TitleDetails;
namespace Turner.UI.Repository
{
    public class TitleRepository : ITitleRepository
    {
        private TitleSearch data = null;
        public TitleRepository()
        {
            data = new TitleSearch();
        }
        public TitleSearchResult SearchTitles(TitleSearchCriteria searchCriteria)
        {
           
            return data.SearchTitle(searchCriteria);
        }


        public List<Award> GetAwardsByTitle(int TitleId)
        {
            return data.GetAwardsByTitle(TitleId);
        }

        public List<StoryLine> GetStoryLinesByTitle(int TitleId)
        {
            return data.GetStoryLinesByTitle(TitleId);
        }

        public List<TitleGenre> GetGenresByTitle(int TitleId)
        {
            return data.GetGenresByTitle(TitleId);
        }

        public List<TitleParticipant> GetParticipantsByTitle(int TitleId)
        {
            return data.GetParticipantsByTitle(TitleId);
        }

        public List<OtherName> GetOtherNamesByTitle(int TitleId)
        {
            return data.GetOtherNamesByTitle(TitleId);
        }
    }
}
