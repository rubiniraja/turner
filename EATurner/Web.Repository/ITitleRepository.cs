﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turner.Entities;
using Turner.Entities.Title;

namespace Turner.UI.Repository
{
    public interface ITitleRepository
    {
        TitleSearchResult SearchTitles(TitleSearchCriteria searchCriteria);

        List<Award> GetAwardsByTitle(int TitleId);

        List<StoryLine> GetStoryLinesByTitle(int TitleId);

        List<TitleGenre> GetGenresByTitle(int TitleId);

        List<TitleParticipant> GetParticipantsByTitle(int TitleId);

        List<OtherName> GetOtherNamesByTitle(int TitleId);
    }
}
