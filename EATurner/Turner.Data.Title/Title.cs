﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// Title.cs
// Contains the code file that has the class that holds a Title
#endregion 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Data.Title
{
    /// <summary>
    /// Represents Title which can be searched / created or updated
    /// </summary>
    public class Title
    {

    }
}
