﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// TitleSearch.cs
// Contains the implementations of Title Search data component
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turner.Entities;
using Turner.Interfaces;
using System.Collections;
using Turner.Data.Model;
using System.Configuration;
using Entities = @Turner.Entities;
using Turner.Entities.Title;


namespace Turner.Data.Model{
    /// <summary>
    /// Class that implements operations on title search
    /// </summary>
    public class TitleSearch : ITitleSearchData
    {
        /// <summary>
        /// The connection string
        /// </summary>
        string connectionString = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleSearch"/> class.
        /// </summary>
        public TitleSearch()
        {
            connectionString = ConfigurationManager.ConnectionStrings["TitleEntities"].ConnectionString;
        }

        /// <summary>
        /// Searches the title.
        /// </summary>
        /// <param name="SearchCriteria">The search criteria.</param>
        /// <returns></returns>
        public TitleSearchResult SearchTitle(TitleSearchCriteria SearchCriteria)
        {
            TitleSearchResult searchResult = new TitleSearchResult();
            List<TitleDetails> titles = null;
            try
            {
             
                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {
                    string sortStr = "titleData." + SearchCriteria.Field;
                   
                    titles = (from titleData in titleContext.Titles
                              where titleData.TitleName.Contains(SearchCriteria.SearchField)
                              orderby titleData.TitleNameSortable descending
                              select new TitleDetails() { TitleId = titleData.TitleId, TitleName = titleData.TitleName, TitleNameSortable = titleData.TitleNameSortable, ProcessedDateTimeUTC = titleData.ProcessedDateTimeUTC, ReleaseYear = titleData.ReleaseYear }).ToList();

                    searchResult.TitleDetails = titles != null
                       ? titles.Skip(SearchCriteria.StartIndex).Take(SearchCriteria.Number).ToList() //Paging
                       : titles;
                    searchResult.TotalSearchCount = titles.Count();
                  
                }
              
            }
            catch (Exception)
            {
                throw;
            }
            return searchResult;
        }






        /// <summary>
        /// Gets the awards by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        public List<Entities.Award> GetAwardsByTitle(int TitleId)
        {
            List<Entities.Award> awards = null;
            try
            {

                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {

                    awards = (from titleData in titleContext.Awards
                              where titleData.TitleId == TitleId
                              select new Entities.Award()
                              {
                                  AwardYear = titleData.AwardYear,
                                  AwardCompany = titleData.AwardCompany,
                                  AwardWon = titleData.AwardWon,
                                  Id = titleData.Id
                              }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return awards;
        }


        /// <summary>
        /// Gets the story lines by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        public List<Entities.StoryLine> GetStoryLinesByTitle(int TitleId)
        {
            List<Entities.StoryLine> storyLine = null;
            try
            {

                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {

                    storyLine = (from titleData in titleContext.StoryLines
                              where titleData.TitleId == TitleId
                                 select new Entities.StoryLine()
                              {
                                  Description = titleData.Description,
                                  Language = titleData.Language,
                                  Type = titleData.Type,
                                  Id = titleData.Id
                              }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return storyLine;
        }

        /// <summary>
        /// Gets the genres by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        public List<Entities.TitleGenre> GetGenresByTitle(int TitleId)
        {
            List<Entities.TitleGenre> genres = null;
            try
            {

                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {

                    genres = (from titleData in titleContext.TitleGenres
                              where titleData.TitleId == TitleId
                              select new Entities.TitleGenre()
                              {
                                  Id = titleData.GenreId,
                                  Name = titleData.Genre.Name,
                                  TitleGenreId = titleData.Id
                              }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return genres;
        }

        /// <summary>
        /// Gets the participants by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        public List<Entities.TitleParticipant> GetParticipantsByTitle(int TitleId)
        {
            List<Entities.TitleParticipant> participants = null;
            try
            {

                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {

                    participants = (from titleData in titleContext.TitleParticipants
                                    where titleData.TitleId == TitleId
                                    select new Entities.TitleParticipant()
                                    {
                                        Id = titleData.Id,
                                        IsKey = titleData.IsKey,
                                        IsOnScreen = titleData.IsOnScreen,
                                        Name = titleData.Participant.Name,
                                        RoleType = titleData.RoleType,
                                        Type = titleData.Participant.ParticipantType
                                    }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return participants;
        }

        /// <summary>
        /// Gets the other names by title.
        /// </summary>
        /// <param name="TitleId">The title identifier.</param>
        /// <returns></returns>
        public List<Entities.OtherName> GetOtherNamesByTitle(int TitleId)
        {
            List<Entities.OtherName> otherNames = null;
            try
            {

                using (TitleEntities titleContext = new TitleEntities(connectionString))
                {

                    otherNames = (from titleData in titleContext.OtherNames
                              where titleData.TitleId == TitleId
                              select new Entities.OtherName()
                              {
                                  Id = titleData.Id,
                                  Language = titleData.TitleNameLanguage,
                                  Name = titleData.TitleName,
                                  Type = titleData.TitleNameType
                              }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }
            return otherNames;
        }
    }
}
