﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// Title.cs
// Contains the code file that has the properties of a Title
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents title info
    /// </summary>
    public class TitleDetails
    {
        /// <summary>
        /// Gets or sets the title identifier.
        /// </summary>
        /// <value>
        /// The title identifier.
        /// </value>
        public int TitleId { get; set; }
        /// <summary>
        /// Gets or sets the name of the title.
        /// </summary>
        /// <value>
        /// The name of the title.
        /// </value>
        public string TitleName { get; set; }
        /// <summary>
        /// Gets or sets the title name sortable.
        /// </summary>
        /// <value>
        /// The title name sortable.
        /// </value>
        public string TitleNameSortable { get; set; }
        /// <summary>
        /// Gets or sets the title type identifier.
        /// </summary>
        /// <value>
        /// The title type identifier.
        /// </value>
        public int TitleTypeId { get; set; }
        /// <summary>
        /// Gets or sets the release year.
        /// </summary>
        /// <value>
        /// The release year.
        /// </value>
        public int? ReleaseYear { get; set; }
        /// <summary>
        /// Gets or sets the processed date time UTC.
        /// </summary>
        /// <value>
        /// The processed date time UTC.
        /// </value>
        public DateTime? ProcessedDateTimeUTC { get; set; }

        /// <summary>
        /// Gets or sets the awards.
        /// </summary>
        /// <value>
        /// The awards.
        /// </value>
        public List<Award> Awards { get; set; }
        /// <summary>
        /// Gets or sets the other names.
        /// </summary>
        /// <value>
        /// The other names.
        /// </value>
        public List<OtherName> OtherNames { get; set; }
        /// <summary>
        /// Gets or sets the story lines.
        /// </summary>
        /// <value>
        /// The story lines.
        /// </value>
        public List<StoryLine> StoryLines { get; set; }
        /// <summary>
        /// Gets or sets the title genres.
        /// </summary>
        /// <value>
        /// The title genres.
        /// </value>
        public List<TitleGenre> TitleGenres { get; set; }
        /// <summary>
        /// Gets or sets the title participants.
        /// </summary>
        /// <value>
        /// The title participants.
        /// </value>
        public List<TitleParticipant> TitleParticipants { get; set; }
    }
}
