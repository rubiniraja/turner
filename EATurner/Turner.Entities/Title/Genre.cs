﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// Genre.cs
// Contains the code file that has the properties of Genre
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents Genre
    /// </summary>
    public class Genre
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
    }
}
