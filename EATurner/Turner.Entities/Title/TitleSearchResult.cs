﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities.Title
{
    /// <summary>
    /// class represents title search result
    /// </summary>
    public class TitleSearchResult
    {
        /// <summary>
        /// Gets or sets the title details.
        /// </summary>
        /// <value>
        /// The title details.
        /// </value>
        public List<Entities.TitleDetails> TitleDetails { get; set; }
        /// <summary>
        /// Gets or sets the total search count.
        /// </summary>
        /// <value>
        /// The total search count.
        /// </value>
        public int TotalSearchCount { get; set; }
    }
}
