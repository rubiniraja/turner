﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents title search criteria
    /// </summary>
    public class TitleSearchCriteria : IPage, ISort, IFilter
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the start index.
        /// </summary>
        /// <value>
        /// The start index.
        /// </value>
        public int StartIndex { get; set; }

        /// <summary>
        /// Gets or sets the end index.
        /// </summary>
        /// <value>
        /// The end index.
        /// </value>
        public int EndIndex { get; set; }

        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        /// <value>
        /// The field.
        /// </value>
        public string Field { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is ascending.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is ascending; otherwise, <c>false</c>.
        /// </value>
        public bool IsAscending { get; set; }

        /// <summary>
        /// Gets or sets the type of the filter.
        /// </summary>
        /// <value>
        /// The type of the filter.
        /// </value>
        public SearchType FilterType { get; set; }

        /// <summary>
        /// Gets or sets the search field.
        /// </summary>
        /// <value>
        /// The search field.
        /// </value>
        public string SearchField { get; set; }

    }
}
