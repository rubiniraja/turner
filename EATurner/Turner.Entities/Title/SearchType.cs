﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents title SearchType
    /// </summary>
    public enum SearchType
    {
        /// <summary>
        /// The contains
        /// </summary>
        Contains,
        /// <summary>
        /// The starts with
        /// </summary>
        StartsWith,
        /// <summary>
        /// The ends with
        /// </summary>
        EndsWith,
        /// <summary>
        /// The exact
        /// </summary>
        Exact
    }
}
