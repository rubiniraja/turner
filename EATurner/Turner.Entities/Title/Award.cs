﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// Award.cs
// Contains the code file that has the properties of an award
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class representing title award
    /// </summary>
    public class Award
    {
        /// <summary>
        /// Gets or sets the award won.
        /// </summary>
        /// <value>
        /// The award won.
        /// </value>
        public bool? AwardWon { get; set; }
        /// <summary>
        /// Gets or sets the award year.
        /// </summary>
        /// <value>
        /// The award year.
        /// </value>
        public int? AwardYear { get; set; }
        /// <summary>
        /// Gets or sets the award1.
        /// </summary>
        /// <value>
        /// The award1.
        /// </value>
        public string Award1 { get; set; }
        /// <summary>
        /// Gets or sets the award company.
        /// </summary>
        /// <value>
        /// The award company.
        /// </value>
        public string AwardCompany { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
    
    }
}
