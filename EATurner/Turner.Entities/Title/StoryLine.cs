﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// StoryLine.cs
// Contains the code file that has the properties of a StoryLine
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents title StoryLine
    /// </summary>
    public class StoryLine
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public string Language { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
    }
}
