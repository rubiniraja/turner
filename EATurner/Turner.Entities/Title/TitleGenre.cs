﻿#region Header
// Copyright © 2014 Turner
// All rights reserved.
//
// Reproduction or transmission, in whole or in part, in any form or
// by any means including electronic or mechanical or otherwise, is 
// prohibited without written permission from Turner.
//
// Award.cs
// Contains the code file that has the properties of a Title Genre
#endregion 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// class represents title Genre
    /// </summary>
    public class TitleGenre : Genre
    {
        /// <summary>
        /// Gets or sets the title genre identifier.
        /// </summary>
        /// <value>
        /// The title genre identifier.
        /// </value>
        public int TitleGenreId { get; set; }
        
    }
}
