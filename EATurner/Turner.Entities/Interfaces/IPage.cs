﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPage
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
         int Number { get; set; }
         /// <summary>
         /// Gets or sets the start index.
         /// </summary>
         /// <value>
         /// The start index.
         /// </value>
         int StartIndex { get; set; }
         /// <summary>
         /// Gets or sets the end index.
         /// </summary>
         /// <value>
         /// The end index.
         /// </value>
         int EndIndex { get; set; }
    }
}
