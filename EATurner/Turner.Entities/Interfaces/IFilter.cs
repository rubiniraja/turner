﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    ///  interface with filter properties
    /// </summary>
    public interface IFilter
    {
        /// <summary>
        /// Gets or sets the search field.
        /// </summary>
        /// <value>
        /// The search field.
        /// </value>
         string SearchField { get; set; }
         /// <summary>
         /// Gets or sets the type of the filter.
         /// </summary>
         /// <value>
         /// The type of the filter.
         /// </value>
         SearchType FilterType { get; set; }
    }
}
