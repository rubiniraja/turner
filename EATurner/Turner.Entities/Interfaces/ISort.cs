﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turner.Entities
{
    /// <summary>
    /// interface with Sorting properties
    /// </summary>
    public interface ISort
    {
        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        /// <value>
        /// The field.
        /// </value>
         string Field { get; set; }
         /// <summary>
         /// Gets or sets a value indicating whether this instance is ascending.
         /// </summary>
         /// <value>
         /// <c>true</c> if this instance is ascending; otherwise, <c>false</c>.
         /// </value>
         bool IsAscending { get; set; }
    }
}
